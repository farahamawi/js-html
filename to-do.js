//To-Do app using HTML and Javascript
//Ashwini Kamath
//Reference: https://www.youtube.com/watch?v=XGOJVfOW-bo&list=PLGJDCzBP5j3xKdHn-RYHvk3uafZSN0eYV&index=23

function toggleBox() {
	var cb_id = this.id.replace("cb_", "");
	var itemText = document.getElementById("sp_" + cb_id);

	if (this.checked){					//if task done, strike off item and fade color
		itemText.style.textDecoration = "line-through";
	} else {
		itemText.style.textDecoration = "none";
	}
}

var item_num = 0;
var task = document.getElementById('task');	
task.focus();
task.onkeyup = function(event){
	if (event.which == 13) {		//when 'ENTER' pressed

		var inBox = document.createElement("li");
		var checkBox = document.createElement("input");
		checkBox.type = 'checkbox';
		checkBox.id = "cb_" + item_num
		checkBox.onclick = toggleBox;

		var span = document.createElement("span");
		span.id = "sp_" + item_num;
		span.innerText = task.value;	

		
		if (span.innerText != ""){			//if textbox not empty

			var list = document.getElementById('tasklist');

			span.style.padding = '10px';

			inBox.appendChild(checkBox);
			inBox.appendChild(span);

			list.appendChild(inBox);

			item_num++;

			document.getElementById('task').value = "";
		}
	}
};
